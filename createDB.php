<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23/10/17
 * Time: 9:47 PM
 */

/**
 * Print Error Statements
 * @param $string
 * @param $con
 */
function printError($string, $con) {
    echo "Error executing \"$string\" ". mysqli_error($con)."\n";
}

/**
 * Print Error Statements
 * @param $query
 */
function printSuccess($query) {
    echo "Successfully Executed \"$query\"\n";
}
$username = readline("Enter Username for MYSQL Database: ");
$password = readline("Enter Password for you MySQL Database: ");
$dbname = "notice";
$create_db_query = "CREATE DATABASE $dbname";
// MySQL Query to create Table notices_internal
$create_notices_query = "CREATE TABLE `notices_internal` (
  `id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `summary` varchar(1000) DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image_file` varchar(200) DEFAULT NULL,
  `pdf_file` varchar(200) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `ctgory` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1";

// MySQL Query to create Table category
$create_cat_query = "CREATE TABLE `category` (
  `cid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1";
/*
 * This part of the sets up the process of creating the Database and the associated Tables
 */
$mysql_con = mysqli_connect("localhost", $username, $password);
if (!$mysql_con) {
    echo "Could not connect to MYSQL for user \"$username\"\n";
} else {
    echo "Successfully connected to database for user \"$username\"\n";
    $exec_cdb = mysqli_query($mysql_con, $create_db_query);
    if ($exec_cdb) {
        printSuccess($create_db_query);
        $db_connect = mysqli_connect("localhost", $username, $password, $dbname);
        if (!$db_connect) {
            echo "Could not connect to database $dbname for user $username\n";
        } else {
            $create_table_notices_internal = mysqli_query($db_connect,$create_notices_query);
            if ($create_table_notices_internal) {
                printSuccess($create_notices_query);
            } else {
                printError($create_notices_query, $db_connect);
            }

            $create_table_category = mysqli_query($db_connect,$create_cat_query);

            if ($create_table_category) {
                printSuccess($create_cat_query);
            } else {
                printError($create_cat_query, $db_connect);
            }
        }
    } else {
        printError($create_db_query, $mysql_con);
    }
    mysqli_close($mysql_con);
}
