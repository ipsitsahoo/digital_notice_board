/**
 * Created by Ipsit Sahoo on 17/9/17.
 */
$(document).ready(function(){
    $('#editSet').modal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal
            opacity: .5, // Opacity of modal background
            inDuration: 300, // Transition in duration
            outDuration: 200, // Transition out duration
            startingTop: '1%', // Starting top style attribute
            endingTop: '10%', // Ending top style attribute
            ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                console.log(modal, trigger);
            }
        });
    notice_category = $("[name='notice-category'] option");
});
function showData(id) {
    $('#editSet').modal('open');
    localStorage.setItem("up_id", id);
    $.ajax({
        url: 'getFormData.php?format=get',
        type: 'post',
        data: {
            notice_id: id
        },
        dataType: "json",
        beforeSend: function () {
            Materialize.toast("Editing Data...", 100);
        },
        success: function (response) {
            $("#editSet").find("h5").html(`EDIT ${response.title}`);
            $("[name='show-notice-title']").val(response.title);
            $("[name='show-notice-summary']").val(response.summary);
            $("[name='show-notice-period']").val(parseInt(response.duration));
            let ctgory = parseInt(response.category);
            $(notice_category).eq(ctgory - 1).attr("selected", true);
            $(".modal-content .select-dropdown").val($(notice_category).eq(ctgory - 1).text());
            let priority = parseInt(response.priority);
            $("[name='show-notice-priority']").eq(priority - 1).attr("checked", true);
            if (response.image !== null) {
                $("#curr_image").prop("href", "./../uploaded_images/" + response.image);
            } else {
                $("#curr_image").prop("href", "javascript:void(0);");
            }
            if (response.pdf !== null) {
                $("#curr_pdf").prop("href", "./../uploaded_PDFs/" + response.pdf);
            } else {
                $("#curr_pdf").prop("href", "javascript:void(0);");
            }
        },
        error: function () {
            Materialize.toast("Connection Error", 2000);
        }
    });
    return false;
}

function submitUpdate(event) {
    // TODO: Fix issue in updating Notice Category!! As of now it does not update
    event.stopPropagation();
    event.preventDefault();
    let obtained_id = localStorage.getItem("up_id");
    let notice_title = $("[name='show-notice-title']").val();
    let notice_summary = $("[name='show-notice-summary']").val();
    let notice_period = $("[name='show-notice-period']").val();
    let notice_category = $("[name='notice-category']").find(":selected").attr("value");
    let notice_priority = null;
    console.log(notice_category);
    $("[name='show-notice-priority']").each(function () {
       if ($(this).is(":checked")) {
           notice_priority = $(this).prop("id");
       }
    });
    if (notice_priority === null) {
        return false;
    }
    let image_file = $("[name='update_image']").prop("files");
    let pdf_file = $("[name='update_pdf']").prop("files");
    let image= null, pdf = null;
    console.log(image_file); console.log(pdf_file);
    if (image_file.length !== 0)
    {
        image = image_file[0];
    }
    if (pdf_file.length !== 0)
    {
        pdf = pdf_file[0];
    }
    console.log(image); console.log(pdf);
    let update_data = new FormData();
    update_data.append("obtained_id", obtained_id);
    update_data.append("notice_title", notice_title);
    update_data.append("notice_period", notice_period);
    update_data.append("notice_priority", notice_priority.split("-")[1]);
    update_data.append("notice_summary", notice_summary);
    update_data.append("notice_category", notice_category);
    update_data.append("notice_if", image);
    update_data.append("notice_ip", pdf);
    $.ajax({
        url: 'getFormData.php?format=edit',
        type: 'post',
        data: update_data,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
            Materialize.toast("Updating Data...", 100);
        },
        success: function (response) {
            console.log(response);
            if (response.id === 200) {
                Materialize.toast(`Updated Data for ${notice_title} Successfully`, 2000);
            } else {
                Materialize.toast(`Failed to Update Data for ${notice_title}`, 2000);
            }
        },
        error: function () {
            Materialize.toast("Connection Error", 2000);
        }
    });

}