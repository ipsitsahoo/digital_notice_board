<?php
/**
 * Created by Ipsit Sahoo.
 * User: root
 * Date: 17/9/17
 * Time: 12:47 PM
 */
?>
<!-- Modal Structure -->
<script type="text/javascript" src="./../js/editModal.js"></script>
<div id="editSet" class="modal">
    <h5 class="center"></h5>
    <form method="POST" action="" enctype="multipart/form-data" onsubmit="submitUpdate(event);">
        <div class="modal-content">
            <div class="card-content">
                <div class="row">
                    <div class="input-field col s12">
                        <blockquote><strong>Notice Title</strong></blockquote>
                        <input type="text" name="show-notice-title" id="notice-title" required>
                    </div>
                </div>
                <div class="row">
                    <blockquote><strong>Notice Summary</strong></blockquote>
                    <div class="input-field col s12">
                        <input type="text" name="show-notice-summary" id="notice-summary" required>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <blockquote><strong>Notice Duration</strong></blockquote>
                        <input type="number" name="show-notice-period" id="notice-period" min="0" required>
                    </div>
                    <div class="input-field col s6">
                        <blockquote><strong>Notice Category</strong></blockquote>
                        <select title="Select a Category" name="notice-category" required>
                            <?php while ($row = mysqli_fetch_assoc($list_category)):?>
                                <option value="<?php echo $row["cid"]?>"><?php echo $row["name"]?></option>
                            <?php endwhile;?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <blockquote><strong>Enter Priority level for the Notice</strong></blockquote>
                        <div>
                            <input type="radio" name="show-notice-priority" id="notice-high">
                            <label for="notice-high">High</label>
                            <input type="radio" name="show-notice-priority" id="notice-medium">
                            <label for="notice-medium">Medium</label>
                            <input type="radio" name="show-notice-priority" id="notice-low">
                            <label for="notice-low">Low</label>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row file_update_area">
                    <blockquote style="text-align: center; font-weight: bolder;">WARNING!! Upload jpg/png type files for images and PDFs for Notices only.</blockquote>
                    <div class="col s2">
                        <a id="curr_image" href="javascript:void(0);" target="_blank">Current Image</a>
                    </div>
                    <div class="col s4">
                        <input type="file" name="update_image">
                    </div>
                    <div class="col s2">
                        <a id="curr_pdf" href="javascript:void(0);" target="_blank">Current PDF</a>
                    </div>
                    <div class="col s4">
                        <input type="file" name="update_pdf">
                    </div>
                </div>
            </div>
            <hr>
            <div class="card-action">
                <div class="center">
                    <button class="btn waves-effect waves-light" type="submit" name="upload_data">Update New Details
                        <i class="material-icons right">file_upload</i>
                    </button>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </form>

</div>
