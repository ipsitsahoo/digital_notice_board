<?php
/**
 * Created by Ipsit Sahoo.
 * User: root
 * Date: 3/9/17
 * Time: 12:05 PM
 */
require_once "FileManager.php";
require_once "connect.php";
$format = $_GET["format"];
if ($_SERVER["REQUEST_METHOD"] == "POST" && $format == "upload")
{
    $fm = new FileManager($mysql_con);
    if (isset($_POST["image"]) && !isset($_POST["file"])) {
        $response = $fm->uploadFile($_REQUEST, $_FILES["file"], null);
    }
    elseif (isset($_POST["file"]) && !isset($_POST["image"]))
    {
        $response = $fm->uploadFile($_REQUEST,null, $_FILES["image"]);
    }
    elseif (isset($_POST["file"]) && isset($_POST["image"]))
    {
        $response = $fm->uploadFile($_REQUEST,null, null);
    }
    else {
        $response = $fm->uploadFile($_REQUEST,$_FILES["file"], $_FILES["image"]);
    }
    echo json_encode($response);
}
if ($_SERVER["REQUEST_METHOD"] == "POST" && $format == "delete")
{
    $fm = new FileManager($mysql_con);
    $id = $_REQUEST["notice_id"];
    $response = $fm->deleteFile($id);
    echo json_encode($response);
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && $format == "get")
{
    $fm = new FileManager($mysql_con);
    $id = $_REQUEST["notice_id"];
    $response = $fm->getFile($id);
    echo json_encode($response);
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && $format == "edit")
{
    $fm = new FileManager($mysql_con);
    $id = $_POST["obtained_id"];
    $title = $_POST["notice_title"];
    $summary = $_POST["notice_summary"];
    $duration = $_POST["notice_period"];
    $priority = $_POST["notice_priority"];
    $catg = (int)($_POST["notice_category"]);
    $image_type_bool = isset($_FILES["notice_if"]) && ($_FILES["notice_if"]["type"] == "image/jpeg" || $_FILES["notice_if"]["type"] == "image/png");
    $pdf_type_bool = isset($_FILES["notice_ip"]) && ($_FILES["notice_ip"]["type"] == "application/pdf");
    $image_file = ($image_type_bool)?$_FILES["notice_if"]:null;
    $pdf_file = ($pdf_type_bool)?$_FILES["notice_ip"]:null;
    if ($pdf_file != null) {
        $response["pdf"] = $fm->updatePDF($id, $pdf_file);
    }
    if($image_file != null) {
        $response["image"] = $fm->updateImage($id, $image_file);
    }
    $response = $fm->updateContents($id, $title, $summary, $duration, $priority, $catg);
    echo json_encode($response);
}