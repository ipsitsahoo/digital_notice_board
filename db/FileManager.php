<?php

/**
 * Created by Ipsit Sahoo.
 * User: root
 * Date: 3/9/17
 * Time: 12:49 PM
 */
require_once "Common.php";
class FileManager extends Common
{
    private $Imagelocation;
    private $PDFlocation;
    private $mysqlCon;
    public $response = array();
    function __construct($con)
    {
        $this->mysqlCon = $con;
        $this->PDFlocation = $this->getUploadDirectory("uploaded_PDFs/");
        $this->Imagelocation = $this->getUploadDirectory("uploaded_images/");
    }
    public function uploadFile($request, $file, $image)
    {
        $listOfData = $this->getCountOfData($this->mysqlCon, "notices_internal") + 1;
        $title = $this->sanitizeTextInput($request["title"]);
        $summary = $this->sanitizeTextInput($request["summary"]);
        $duration = $this->sanitizeTextInput($request["duration"]);
        $priority = $this->sanitizePriority($request["priority"]);
        $category = (int)$this->sanitizeTextInput($request["category"]);
        // Data Upload Segment
        $file_name = null;
        $image_name = null;$isPDFUploaded = false;$isImageUploaded = false;
        $query = "INSERT INTO notices_internal(title, summary, duration, priority, ctgory) VALUES ('$title', '$summary', $duration, $priority, $category)";
        // If we have the PDF File
        if ($file != null) {
            $file_name = $listOfData."_".$file["name"];
            $isPDFUploaded = move_uploaded_file($file['tmp_name'], $this->PDFlocation . $file_name);
            $query = ($isPDFUploaded)?"INSERT INTO notices_internal(title, summary, duration, priority, pdf_file) 
VALUES ('$title', '$summary', $duration, $priority, '$file_name')": $query;
        }
        // if We have Image File
        if ($image != null) {
            $image_name = $listOfData."_".$image["name"];
            $isImageUploaded = move_uploaded_file($image['tmp_name'], $this->Imagelocation . $image_name);
            $query = "INSERT INTO notices_internal(title, summary, duration, priority, image_file) 
VALUES ('$title', '$summary', $duration, $priority, '$image_name')";
        }
        // if both are present
        if ($file != null && $image != null)
        {
            $query = ($isImageUploaded)?"INSERT INTO notices_internal(title, summary, duration, priority, pdf_file, image_file) 
VALUES ('$title', '$summary', $duration, $priority, '$file_name', '$image_name')": $query;
        }

        // Perform Database operations based on uploaded images and files
        if ($isImageUploaded || $isPDFUploaded)
        {

            $isInserted = mysqli_query($this->mysqlCon, $query);
            if ($isInserted)
            {
                $this->response["id"] = 200;
                $this->response["msg"] = "Uploaded and Saved Successfully";
            }
            else
            {
                $this->response["id"] = 300;
                $this->response["msg"] = mysqli_error($this->mysqlCon);
                $this->writeLogs("dblogs.txt", mysqli_error($this->mysqlCon));
            }
        }
        else
        {
            $this->response["id"] = 300;
            $this->response["msg"] = "Error in Uploading File. Please try After Some Time";
        }
        return $this->response;
    }

    public function deleteFile($id)
    {
        $id = (int)$id;
        $condition = array();
        $condition[0] = "id"; $condition[1] = $id;
        $exec = $this->getListofDataFromDB($this->mysqlCon, "notices_internal", $condition);
        if ($exec)
        {
            $row = mysqli_fetch_assoc($exec);
            unlink($this->Imagelocation.$row["image_file"]);
            unlink($this->PDFlocation.$row["pdf_file"]);
            $query = "DELETE FROM notices_internal WHERE id = $id";
            $delete_exec = mysqli_query($this->mysqlCon, $query);
            if ($delete_exec) {
                $this->response["id"] = 200;
                $this->response["msg"] = "Deleted Successfully";
            }
            else
            {
                $this->response["id"] = 300;
                $this->response["msg"] = "Error in Deleting File. Please try After Some Time";
                $this->writeLogs("dblogs.txt", mysqli_error($this->mysqlCon));
            }
        }
        else
        {
            $this->response["id"] = 300;
            $this->response["msg"] = "Error in Deleting File Contents. Please try After Some Time";
            $this->writeLogs("dblogs.txt", mysqli_error($this->mysqlCon));
        }
        return $this->response;
    }

    public function getFile($id)
    {
        $id = (int)$id;
        $condition = array();
        $condition[0] = "id"; $condition[1] = $id;
        $exec = $this->getListofDataFromDB($this->mysqlCon, "notices_internal", $condition);
        if ($exec)
        {
            $row = mysqli_fetch_assoc($exec);
            $this->response["id"] = 200;
            $this->response["msg"] = "Obtained Data Successfully";
            $this->response["title"] = $row["title"];
            $this->response["summary"]= $row["summary"];
            $this->response["duration"] = $row["duration"];
            $this->response["priority"] = $row["priority"];
            $this->response["image"] = $row["image_file"];
            $this->response["category"] = $row["ctgory"];
            $this->response["pdf"] = $row["pdf_file"];
        }
        else
        {
            $this->response["id"] = 300;
            $this->response["msg"] = "Error in Getting File Contents. Please try After Some Time";
            $this->writeLogs("dblogs.txt", mysqli_error($this->mysqlCon));
        }
        return $this->response;
    }

    public function updateContents($id, $title, $summary, $duration, $priority, $category = 1) {
        $id = (int)$id;
        $title = $this->sanitizeTextInput($title);
        $summary = $this->sanitizeTextInput($summary);
        $duration = (int)$duration;
        $priority = $this->sanitizePriority($priority);
        $query = "UPDATE notices_internal SET summary = '$summary', duration = $duration, 
title = '$title', priority = $priority, ctgory = $category WHERE id = $id";
        $exec = mysqli_query($this->mysqlCon, $query);
        if ($exec) {
            $this->response["id"] = 200;
            $this->response["msg"] = "Updated File Contents SuccessFully";
        } else {
            $this->response["id"] = 300;
            $this->response["msg"] = "Error in Updating File Contents for $title. Please try After Some Time";
            $this->writeLogs("dblogs.txt", mysqli_error($this->mysqlCon));
        }
        return $this->response;
    }

    public function updateImage($id, $image) {
        $image["name"] = $id."_".$image["name"];
        $image_name = $image["name"];
        $isUploaded = move_uploaded_file($image["tmp_name"], $this->Imagelocation.$image_name);
        if ($isUploaded) {
            $this->writeLogs("dblogs.txt", "Added Image Location is: ".$this->PDFlocation.$image_name);
            $get_query = "SELECT image_file FROM notices_internal WHERE id = $id";
            $res = mysqli_query($this->mysqlCon, $get_query);
            $row = mysqli_fetch_assoc($res);
            if (isset($row["image_file"])) {
                $this->writeLogs("dblogs.txt", "Deleting: ".$row["image_file"]);
                unlink("./../uploaded_images/" . $row["image_file"]);
            }
            $update_query = "UPDATE notices_internal SET image_file = '$image_name' WHERE id = $id";
            $res = mysqli_query($this->mysqlCon, $update_query);
            return $res && mysqli_affected_rows($this->mysqlCon);
        } else {
            $this->writeLogs("dblogs.txt", "Image Location is: ".$this->Imagelocation.$image_name);
            return false;
        }
    }

    public function updatePDF($id, $pdf) {
        $pdf["name"] = $id."_".$pdf["name"];
        $pdf_name = $pdf["name"];
        $isUploaded = move_uploaded_file($pdf["tmp_name"], $this->PDFlocation.$pdf_name);
        if ($isUploaded) {
            $this->writeLogs("dblogs.txt", "Added PDF! Location: ".$this->PDFlocation.$pdf_name);
            $get_query = "SELECT pdf_file FROM notices_internal WHERE id = $id";
            $res = mysqli_query($this->mysqlCon, $get_query);
            $row = mysqli_fetch_assoc($res);
            if ($row["pdf_file"]) {
                $this->writeLogs("dblogs.txt", "Deleting: ".$row["pdf_file"]);
                unlink("./../uploaded_PDFs/" . $row["pdf_file"]);
            }
            $update_query = "UPDATE notices_internal SET pdf_file = '$pdf_name' WHERE id = $id";
            $res = mysqli_query($this->mysqlCon, $update_query);
            return $res && mysqli_affected_rows($this->mysqlCon);
        } else {
            $this->writeLogs("dblogs.txt", "PDF Location is: ".$this->PDFlocation.$pdf_name);
            return false;
        }
    }
}