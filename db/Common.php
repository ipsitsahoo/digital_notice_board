<?php

/**
 * Created by Ipsit Sahoo.
 * User: root
 * Date: 3/9/17
 * Time: 12:58 PM
 */
class Common
{
    public function sanitizeTextInput($data)
    {
        $data = trim($data);
        $data = addslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    public function sanitizePriority($data)
    {
        $data = strtolower($data);
        $datar = 0;
        if ($data == "low")
        {
            $datar = 3;
        }
        elseif ($data == "medium")
        {
            $datar = 2;
        }
        elseif ($data == "high")
        {
            $datar = 1;
        }
        return $datar;
    }
    public function getCountOfData($con, $table)
    {
        $query = "SELECT count(*) FROM ".$table;
        $isObtained = mysqli_query($con, $query);
        if ($isObtained)
        {
            $data = mysqli_fetch_assoc($isObtained);
            return (int)$data["count(*)"];
        }
        return -1;
    }
    public function getUploadDirectory($folder)
    {
        $curr = getcwd();
        $data = explode("/", $curr);
        $len = sizeof($data);
        $data[$len - 1] = $folder;
        return implode("/", $data);
    }
    public function getListofDataFromDB($con, $table, $condition)
    {
        $query = NULL;
        if ($condition == NULL) {
            $query = "SELECT * FROM ".$table;
        }
        else {
            if (gettype($condition[1]) == "integer") {
                $query = "SELECT * FROM " . $table . " WHERE $condition[0] = $condition[1]";
            }
            elseif (gettype($condition[1]) == "string"){
                $query = "SELECT * FROM " . $table . " WHERE $condition[0] = '$condition[1]'";
            }
        }
        $listData = mysqli_query($con, $query);
        return $listData;
    }
    public function writeLogs($file, $string)
    {
        $currentLoc = getcwd();
        $date = date("Y-m-d H:m:s A");
        $loc_arr = explode("/", $currentLoc);
        $loc_arr_len = sizeof($loc_arr);
        $found = $loc_arr_len;
        for ($i = 0; $i < $loc_arr_len; $i++)
        {
            if ($loc_arr[$i] == "notice"){ $found = $i + 1;}
            if ($i >= $found) {$loc_arr[$i] = null;}
        }
        $loc_arr[$found] = "logs/".$file;
        $res = implode("/", $loc_arr);
        $openFile = fopen($res, "a") or die("Could not open File => $file");
        $log_text = $date. " ==> ".$string."\n";
        fwrite($openFile, $log_text);
    }
}