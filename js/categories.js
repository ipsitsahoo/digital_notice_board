/**
 * Created by Ipsit Sahoo on 8/9/17.
 */
function addCategory() {
    event.preventDefault();
    let name = prompt("Enter new category name");
    if (name.length > 0) {
        $.ajax({
            url: "CategoryManager.php",
            type: "POST",
            data: {
                func: "add",
                cat_name: name
            },
            dataType: "json",
            beforeSend: function () {
                Materialize.toast("Adding Category", 1000);
            },
            success: function (response) {
                console.log(response);
                if (response["id"] === 200) {
                    Materialize.toast(response["msg"], 3000);
                } else {
                    Materialize.toast(response["msg"], 3000);
                }
                location.reload(false);
            },
            error: function () {
                Materialize.toast("Server Error!!", 3000);
            }
        });
    }
}

function editCategory(id, old_name) {
    event.preventDefault();
    let name = prompt("Enter new name for the category");
    if (name.toLowerCase() === old_name.toLowerCase()) {
        Materialize.toast("The category name specified is same", 5000);
        return false;
    }
    let len = name.length;
    if(len > 0)
    {
        $.ajax({
            url: "CategoryManager.php",
            type: "POST",
            data: {
                func: "edit",
                cat_id: id,
                cat_name: name
            },
            dataType: "json",
            beforeSend: function () {
                Materialize.toast("Editing Category", 1000);
            },
            success: function (response) {
                console.log(response);
                if (response["id"] === 200) {
                    Materialize.toast(response["msg"], 3000);
                } else {
                    Materialize.toast(response["msg"], 3000);
                }
                location.reload(false);
            },
            error: function () {
                Materialize.toast("Server Error!!", 3000);
            }
        });
    }
}
function deleteCategory(id) {
    event.preventDefault();
    let res = confirm("Are you sure you want to delete ?");
    if (res) {
        $.ajax({
            url: "CategoryManager.php",
            type: "POST",
            data: {
                func: "delete",
                cat_id: id
            },
            dataType: "json",
            beforeSend: function () {
                Materialize.toast("Deleting Category", 1000);
            },
            success: function (response) {
                console.log(response);
                if (response["id"] === 200) {
                    Materialize.toast(response["msg"], 3000);
                } else {
                    Materialize.toast(response["msg"], 3000);
                }
                location.reload(false);
            },
            error: function () {
                Materialize.toast("Server Error!!", 3000);
            }
        });
    }
}
