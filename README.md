### NOTICE BOARD MANAGER

** A web interface for managing Notices in an organization**

#### User View Features

- View Notices sorted based on Priority and Categories

#### Admin Features

- Upload notices by adding NOTICE title, summary, priority, category, either image or PDF file

- Edit or Delete Notices

- Get Notifications regarding New Notices

### HOW TO GET STARTED IN CONTRIBUTING

1. Clone the repository to your desired location which you can access via your localhost. If you are using Linux 
you can put it in /var/www/html folder.

2. Make sure you have Php, MySQL installed in your system

3. Go into the repository Folder and execute the following command **php createDB.php**

4. After this go db/config.php and change the USER and PASSWD to username and password for your MYSQL DB

5. After you should be able to view the website at localhost/<*your repository folder name*>!!

6. Admin Page can be accessed from localhost/<*your repository folder name*>/db/admin.php if you have created your server for accessing that repository folder 
