<?php

/**
 * Created by Ipsit Sahoo.
 * User: root
 * Date: 8/9/17
 * Time: 1:34 PM
 */
require_once "connect.php";
require_once "Common.php";
class CategoryManager extends Common
{
    private $DBcon;
    public $json_data = array();
    function __construct($con)
    {
        $this->DBcon = $con;
    }
    public function deleteCategory($id)
    {
        $select_query = $this->getListofDataFromDB($this->DBcon, "category", array("cid", (int)$id));
        $row_res = mysqli_fetch_assoc($select_query);
        $query = "DELETE FROM category WHERE cid = $id";
        $exec = mysqli_query($this->DBcon, $query);
        if ($exec) {
            $this->json_data["id"] = 200;
            $this->json_data["msg"] = "Successfully Deleted ". $row_res["name"];
            $this->writeLogs("categoryMods.txt", "A category was deleted: ". $row_res["name"]);
        }
        else {
            $this->json_data["id"] = 200;
            $this->json_data["msg"] = "Failed to Delete ". $row_res["name"];
            $this->writeLogs("dblogs.txt", mysqli_error($this->DBcon));
        }
        return json_encode($this->json_data);
    }
    public function editCategory($id, $name)
    {
        $name = $this->sanitizeTextInput($name);
        $select_query = $this->getListofDataFromDB($this->DBcon, "category", array("cid", (int)$id));
        $row_res = mysqli_fetch_assoc($select_query);
        $query = "UPDATE category SET name = '$name' WHERE cid = $id";
        $exec = mysqli_query($this->DBcon, $query);
        if ($exec && mysqli_affected_rows($this->DBcon) > 0) {
            $this->json_data["id"] = 200;
            $this->json_data["msg"] = "Successfully Edited ". $row_res["name"]. " as ". $name;
            $this->writeLogs("categoryMods.txt", "Category ". $row_res["name"]. " was edited: ". $name);
        }
        else {
            $this->json_data["id"] = 300;
            $this->json_data["msg"] = "Failed to edit ". $row_res["name"];
            $this->writeLogs("dblogs.txt", mysqli_error($this->DBcon));
        }
        return $this->json_data;
    }
    public function addCategory($name)
    {
        $name = $this->sanitizeTextInput($name);
        $query = "INSERT INTO category(name) VALUES ('$name')";
        $exec = mysqli_query($this->DBcon, $query);
        if ($exec) {
            $this->json_data["id"] = 200;
            $this->json_data["msg"] = "Successfully Added ". $name;
            $this->writeLogs("categoryMods.txt", "A new Category was added: ". $name);
        }
        else {
            $this->json_data["id"] = 300;
            $this->json_data["msg"] = "Failed to add ". $name;
            $this->writeLogs("dblogs.txt", mysqli_error($this->DBcon));
        }
        return $this->json_data;
    }
}
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $cm = new CategoryManager($mysql_con);
    $format = strtolower($_POST["func"]);
    if ($format == "delete") {
        $id = (int)$_POST["cat_id"];
        echo json_encode($cm->deleteCategory($id));
    } elseif ($format == "edit") {
        $id = (int)$_POST["cat_id"];
        $name = strtoupper($_POST["cat_name"]);
        echo json_encode($cm->editCategory($id, $name));
    } elseif ($format == "add") {
        $name = strtoupper($_POST["cat_name"]);
        echo json_encode($cm->addCategory($name));
    } else {
        echo "Incorrect Parameters";
    }
}