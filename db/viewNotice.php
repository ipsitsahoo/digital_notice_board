<?php
/**
 * Created by Ipsit Sahoo.
 * User: root
 * Date: 3/9/17
 * Time: 4:07 PM
 */
require_once "connect.php";
$query = "SELECT * FROM notices_internal, category WHERE notices_internal.ctgory = category.cid ORDER BY priority";
if (isset($_GET["viewBy"]) && $_GET["viewBy"] != "View All") {
    $res = $_GET["viewBy"];
    $query = "SELECT * FROM notices_internal, category WHERE category.name = '$res' AND notices_internal.ctgory = category.cid";
}
$exec_query = mysqli_query($mysql_con, $query);
$role = $_GET["role"];
require_once "Common.php";
$commonObj = new Common();
$list_category = $commonObj->getListofDataFromDB($mysql_con, "category", NULL);
$list_category_1 = $commonObj->getListofDataFromDB($mysql_con, "category", NULL);
?>
<!doctype html>
<html>
<head>
    <title>Internal Notices</title>
    <link rel="shortcut icon" type="image/x-icon" href="./../favicon.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="./../css/materialize.min.css">
    <link rel="stylesheet" href="./../css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="./../js/jquery.js"></script>
    <script type="text/javascript" src="./../js/materialize.min.js"></script>
    <script type="text/javascript" src="./../js/script.js"></script>
</head>
<body class="grey lighten-4">
<header>
    <nav>
        <div class="nav-wrapper">
            <a href="./../index.php" class="brand-logo">&nbsp;NOTICE BOARD MANAGER</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="viewNotice.php?role=view">View as User</a></li>
                <li><a href="admin.php">Upload Notice</a></li>
                <li><a href="viewNotice.php?role=delete">Modify Notices</a></li>
                <li><a href="javascript:void(0);">Logout</a></li>
            </ul>
        </div>
    </nav>
</header>
<section class="container view-notice">
    <?php require "editModal.php";?>
    <?php if (isset($_GET["role"])):?>
        <div class="row">
        <div class="col s12 m12">
            <div class="card grey lighten-5">
                <div class="card-content">
                    <div class="row">
                        <div class="col s10">
                            <h2 class="card-title center">GENERAL NOTICE-BOARD</h2>
                        </div>
                        <div class="col s2" id="view-by-options">
                            <select title="Select a Category" name="view-by-options">
                                <option value="0" selected>View All</option>
                                <?php while ($row = mysqli_fetch_assoc($list_category_1)):?>
                                    <option value="<?php echo $row["cid"]?>"><?php echo $row["name"]?></option>
                                <?php endwhile;?>
                            </select>
                        </div>
                    </div>
                    <?php if (mysqli_num_rows($exec_query) > 0):?>
                        <ul class="collapsible collection" data-collapsible="accordion">
                        <?php while ($row = mysqli_fetch_assoc($exec_query)):?>
                        <li>
                            <div class='collapsible-header'>
                                <h6 class="center-align">
                                    <span>
                                        <span class='prno_<?php echo $row["priority"];?>'>&nbsp;</span>
                                        &nbsp;
                                        <?php echo $row["title"]?>
                                    </span>
                                    <?php if ($role == "delete"):?>
                                        <span class="badge" onclick="deleteFile(<?php echo $row["id"];?>)">
                                            <i class="material-icons red-text">delete</i>
                                        </span>
                                        <span class="badge" onclick="return showData(<?php echo $row["id"];?>)">
                                            <i class="material-icons teal-text">edit</i>
                                        </span>
                                    <?php endif;?>
                                </h6>
                                <?php if (isset($row["name"])):?>
                                    <span class="new badge cyan" data-badge-caption="<?php echo $row["name"];?>"></span>
                                <?php else:?>
                                    <span class="new badge red" data-badge-caption="NONE ASSIGNED"></span>
                                <?php endif;?>
                            </div>
                            <div class="collapsible-body">
                                <blockquote>Description:<span class="badge right">Published Date: <?php echo $row["timestamp"];?></span></blockquote>
                                <p><?php echo $row["summary"]?></p>
                                <?php if ($row["image_file"] != null):?>
                                    <blockquote>Attached Image:</blockquote>
                                    <img class="responsive-img" src="./../uploaded_images/<?php echo $row["image_file"];?>" style="max-width: 50%;">
                                <?php endif;?>
                                <?php if ($row["pdf_file"] != null):?>
                                    <blockquote>Attached PDF File:</blockquote>
                                    <a href="./../uploaded_PDFs/<?php echo $row["pdf_file"];?>" target="_blank">
                                        PDF for the Notice
                                    </a>
                                <?php endif;?>
                            </div>
                        </li>
                        <?php endwhile;?>
                    </ul>
                    <?php else:?>
                        <div class="card-image center">
                            <span><i class="material-icons red-text" style="font-size: 50px;">warning</i></span>
                        </div>
                        <div class="card-stacked">
                            <div class="card-content">
                                <p class="center">There are no notices yet!!</p>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
    <?php endif;?>
</section>
</body>
</html>
