<?php
/**
 * Created by Ipsit Sahoo.
 * User: root
 * Date: 3/9/17
 * Time: 9:28 AM
 */
require_once "connect.php";
require_once "Common.php";
$commonObj = new Common();
$list_category = $commonObj->getListofDataFromDB($mysql_con, "category", NULL);
?>
<!doctype html>
<html>
<head>
    <title>Admin Page</title>
    <link rel="shortcut icon" type="image/x-icon" href="./../favicon.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="./../css/materialize.min.css">
    <link rel="stylesheet" href="./../css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="./../js/jquery.js"></script>
    <script type="text/javascript" src="./../js/materialize.min.js"></script>
    <script type="text/javascript" src="./../js/script.js"></script>
    <script type="text/javascript" src="./../js/categories.js"></script>
</head>
<body class="grey lighten-4">
    <header>
        <nav>
            <div class="nav-wrapper">
                <a href="./../index.php" class="brand-logo">&nbsp;NOTICE BOARD MANAGER</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="viewNotice.php?role=view">View as User</a></li>
                    <li><a href="viewNotice.php?role=delete">Modify Notices</a></li>
                    <li><a href="javascript:void(0);">Logout</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <section class="container admin">
        <!-- Form for Uploading Notices-->
        <div class="row">
            <div class="col s12 m12">
                <div class="card grey lighten-5">
                    <form method="POST" action="" enctype="multipart/form-data" onsubmit="submitData(event);">
                        <div class="card-content">
                            <h2 class="card-title center">Upload Notices</h2>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="text" name="notice-title" id="notice-title" required>
                                    <label for="notice-title">Enter Title of the Notice</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="text" name="notice-summary" id="notice-summary" required>
                                    <label for="notice-summary">Enter a brief summary of your notice(200 chars. max)</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s4">
                                    <input type="number" name="notice-period" id="notice-period" min="0" required>
                                    <label for="notice-period">Enter a duration for your notice(in days)</label>
                                </div>
                                <div class="input-field col s4">
                                    <select title="Select a Category" name="notice-category" required>
                                        <option value="0" disabled selected>Select a Category</option>
                                        <?php while ($row = mysqli_fetch_assoc($list_category)):?>
                                            <option value="<?php echo $row["cid"]?>"><?php echo $row["name"]?></option>
                                        <?php endwhile;?>
                                    </select>
                                </div>
                                <div class="col s4">
                                    <blockquote>Enter Priority level for the Notice</blockquote>
                                    <div>
                                        <input type="radio" name="notice-priority" id="notice-high">
                                        <label for="notice-high">High</label>
                                        <input type="radio" name="notice-priority" id="notice-medium">
                                        <label for="notice-medium">Medium</label>
                                        <input type="radio" name="notice-priority" id="notice-low" checked>
                                        <label for="notice-low">Low</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s5">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span><i class="material-icons left">attach_file</i>PDF</span>
                                            <input type="file" name="uploaded_file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-field col s5 offset-s2">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span><i class="material-icons left">image</i> Image</span>
                                            <input type="file" name="uploaded_image">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <div class="center">
                                <button class="btn waves-effect waves-light" type="submit" name="upload_data">Upload Details
                                    <i class="material-icons right">file_upload</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Edit the categories in the notices-->
        <?php $list_category = $commonObj->getListofDataFromDB($mysql_con, "category", NULL);?>
        <div class="card grey lighten-5">
            <div class="card-content">
                <a class="btn-floating red right" onclick="addCategory();"><i class="material-icons tiny light-blue">add</i></a>
                <h2 class="card-title center">EDIT CATEGORIES</h2>
                <?php if (mysqli_num_rows($list_category) > 0):?>
                    <ul class="collection">
                        <?php while ($row = mysqli_fetch_assoc($list_category)):?>
                            <ul class="collection-item">
                                <span><?php echo $row["name"]?></span>
                                <a href="#" class="secondary-content"><i class="material-icons red-text" onclick="deleteCategory(<?php echo $row["cid"];?>);">delete</i></a>
                                <a href="#" class="secondary-content"><i class="material-icons" onclick="editCategory(<?php echo $row["cid"];?>, '<?php echo $row["name"];?>');">edit</i></a>
                            </ul>
                        <?php endwhile;?>
                    </ul>
                <?php else:?>
                    <div class="card-image center">
                        <span><i class="material-icons red-text" style="font-size: 50px;">warning</i></span>
                    </div>
                    <div class="card-stacked">
                        <div class="card-content">
                            <p class="center">There are no categories yet!!</p>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </section>
</body>
</html>
