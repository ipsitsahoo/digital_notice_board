<?php
/**
 * Created by Ipsit Sahoo.
 * User: root
 * Date: 3/9/17
 * Time: 9:18 AM
 */
require_once "config.php";
$mysql_con = mysqli_connect(DBSERVER, USER, PASSWD, DBNAME);
if (!$mysql_con)
{
    die("Connection could not be established");
}