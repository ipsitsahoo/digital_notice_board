/**
 * Created by Ipsit Sahoo on 3/9/17.
 */
let max_file_size = 8388608;
$(document).ready(function() {
    $('select').material_select();
    let isViewByThere = false;
    let options = $("[name='view-by-options'] option");
    if (localStorage.hasOwnProperty("viewBy")) {
        let res = localStorage.getItem("viewBy");
        console.log(res);
        $(options).each(function () {
           $(this).attr("selected", false);
        });
        $(options).eq(parseInt(res)).attr("selected", true);
        $("#view-by-options").find(".select-wrapper").find(".select-dropdown").val($(options).eq(parseInt(res)).text());
    }
    $("[name='view-by-options']").change(function () {
       let curr_url = window.location.href;
       let hostName = curr_url.split("?")[0];
       let option_val = $(this).find(":selected").text();
       let get_values = (curr_url.indexOf("?") !== -1) ? curr_url.split("?")[1].split("&") : ["viewBy=" + option_val];
       console.log(get_values);
       for(i = 0; i < get_values.length; i++)
       {
           if (get_values[i].indexOf("viewBy") !== -1) {
               localStorage.setItem("viewBy", $(this).find(":selected").attr("value"));
               get_values[i] = "viewBy=" + option_val;
               isViewByThere = true;
           }
           hostName += (i === 0)? ("?" + get_values[i]): ("&" + get_values[i]);
       }
       console.log(isViewByThere);
       if (!isViewByThere) {
           hostName += ("&" + "viewBy=" + option_val);
       }
       window.setTimeout(function () {
           window.location.href = hostName;
       }, 1000);
    });
});
function submitData(event) {
    event.stopPropagation();
    event.preventDefault();
    let title = $("[name='notice-title']").val();
    let summ = $("[name='notice-summary']").val();
    let durr = $("[name='notice-period']").val();
    let prior = $("[name='notice-priority']:checked").attr("id");
    let catg = $("[name='notice-category']").find(":selected").attr("value");
    let obtained_priority = "low";
    if (prior !== undefined)
    {
        obtained_priority = prior.split('-')[1];
    }
    let image_data = $("[name='uploaded_image']").prop("files");
    let file_data = $("[name='uploaded_file']").prop("files");
    /*console.log(title);
    console.log(summ);
    console.log(durr);*/
    console.log(image_data);
    console.log(file_data);
    /*console.log(obtained_priority);*/
    let file = null, image = null;
    if (image_data.length !== 0)
    {
        image = image_data[0];
    }
    if (file_data.length !== 0)
    {
        file = file_data[0];
    }
    // Form the data
    let data = new FormData();
    data.append('title', title);
    data.append('summary', summ);
    data.append('duration', durr);
    data.append('priority', obtained_priority);
    data.append('category', catg);
    data.append('file', file);
    data.append('image', image);
    $.ajax({
        url: 'getFormData.php?format=upload',
        type: 'post',
        data: data,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
            Materialize.toast("Uploading Data...", 100);
        },
        success: function (response) {
            console.log(response);
            Materialize.toast(response["msg"], 20000);
        },
        error: function () {
            Materialize.toast("Connection Error", 2000);
        }
    });
}

function deleteFile(id) {
    event.preventDefault();
    let res = confirm("Are you sure you want to delete ?");
    if (res) {
        $.ajax({
            url: 'getFormData.php?format=delete',
            type: 'post',
            data: {
                notice_id: id
            },
            dataType: "json",
            beforeSend: function () {
                Materialize.toast("Deleting Data...", 100);
            },
            success: function (response) {
                Materialize.toast(response["msg"], 10000);
                window.setTimeout(function () {
                    location.reload(false);
                }, 5000);
            },
            error: function () {
                Materialize.toast("Connection Error", 2000);
            }
        });
    }
    return false;
}
